FROM debian:11 as builder

ARG SIMPLESAMLPHP_VERSION=1.19.3

RUN apt-get update && \
    apt-get -y install curl ca-certificates --no-install-recommends && \
    apt-get clean && \
    rm -r /var/lib/apt/lists/*

RUN curl -s -L -o /tmp/simplesamlphp.tar.gz https://github.com/simplesamlphp/simplesamlphp/releases/download/v$SIMPLESAMLPHP_VERSION/simplesamlphp-$SIMPLESAMLPHP_VERSION.tar.gz
RUN tar xzf /tmp/simplesamlphp.tar.gz -C /tmp
RUN rm -f /tmp/simplesamlphp.tar.gz
RUN mkdir -p /var/www
RUN mv /tmp/simplesamlphp-* /var/www/simplesamlphp

# Use env var to define sp config
COPY simplesamlphp/saml20-sp-remote.php /var/www/simplesamlphp/metadata/saml20-sp-remote.php

# Use env var to define salt and admin password
COPY simplesamlphp/config.php /var/www/simplesamlphp/config

# Use env var to define users
COPY simplesamlphp/authsources.php /var/www/simplesamlphp/config

# Actiate module exampleauth
RUN touch /var/www/simplesamlphp/modules/exampleauth/enable


FROM php:7.4-apache

COPY --from=builder /var/www/simplesamlphp /var/www/simplesamlphp
RUN chown -R 33:33 /var/www/simplesamlphp
RUN chmod -R u+rwX,g+rX,o+rX /var/www/simplesamlphp

COPY apache/simplesamlphp.conf /etc/apache2/sites-enabled
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
    a2dissite 000-default.conf
