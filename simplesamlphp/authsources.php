<?php

$config = array(
    'admin' => array(
        'core:AdminPassword',
    ),
    'example-userpass' => array(
        'exampleauth:UserPass',
    ),
);

function getUsersFromEnv() {
    $users = [];
    foreach(json_decode(getenv('SIMPLESAMLPHP_USERS'), true) as $user) {
        $users[$user["user"] . ":" . $user["password"]] = [];
        foreach($user["attributs"] as $att) {
            $users[$user["user"] . ":" . $user["password"]][$att["key"]] = $att["value"];
        }
    }
    return $users;
}

$config['example-userpass'] = array_merge($config['example-userpass'], getUsersFromEnv());
