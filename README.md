# Simplesamlphp docker image

Simple project to build docker image for simplesamlphp.

## Usage

Docker-compose example:

```yaml
version: '3.8'
services:
  simplesamlphp:
    image: ""
    volumes:
      - ./simplesamlphp/server.crt:/var/www/simplesamlphp/cert/server.crt
      - ./simplesamlphp/server.pem:/var/www/simplesamlphp/cert/server.pem
    environment:
      SIMPLESAMLPHP_SP_ENTITY_ID: http://localhost:8080
      SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE: http://localhost:8080/saml/login_check
      SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE: http://localhost:8080/saml/logout
      SIMPLESAMLPHP_ADMIN_PASSWORD: password
      SIMPLESAMLPHP_SECRET_SALT: salt
      SIMPLESAMLPHP_USERS: |
        [
            {
                "user": "admin",
                "password": "admin",
                "attributs": [
                    {
                        "key": "email",
                        "value": "admin@test.lan"
                    }
                ]
            }
        ]
    ports:
     - "8085:80"
```
